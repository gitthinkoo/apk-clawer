#1 项目介绍

该工程为maven工程，主要用于从各大Android应用市场上爬取apk文件下载到本地。

#2 程序入口介绍

由于项目要求需要，本过程有两个main函数，即：

    * com.yuyifei.main.Main
    * com.yuyifei.classify.ClassifyMain

##1 com.yuyifei.main.Main

多线程实现，根据给定的关键字，从各大应用市场爬取指定的apk文件。

##2 com.yuyifei.classify.ClassifyMain

单线程实现，从各大市场上分类爬取apk文件。