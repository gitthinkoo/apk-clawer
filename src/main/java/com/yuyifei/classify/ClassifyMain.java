package com.yuyifei.classify;

public class ClassifyMain 
{
	public static void main (String[] args)
	{
		new ClassifyHuaweiHtmlParser().parserHtml();//华为市场
		new ClassifyJiFengHtmlParser().parserHtml();//机峰市场
		
		/*
		 * 可以不启动这个爬虫，百度应用市场的怕取非常慢，而且经常出错
		 */
		//new ClassifyBaiDuHtmlParser().parserHtml();
	}
}
