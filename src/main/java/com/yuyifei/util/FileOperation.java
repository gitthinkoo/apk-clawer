package com.yuyifei.util;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

public class FileOperation 
{
	public static void creatDir(String fileName)
	{
		File file = new File(fileName);
		
		//dir exist, detele it
		if (file.exists())
		{
			//System.out.println("dir exist.");
			return;
		}
		
		//create dir
		if (!file.mkdirs())
		{
			System.err.println("create dir failed.");
			System.exit(1);
		}
	}
}
